import sys
import traceback

from fsl_sienax.parser import parse_config
from fsl_sienax.main import run
from fsl_sienax.xnat_logging import stdout_log, stderr_log

def main():
    """main module"""

    # parse input args
    cmd_to_run = parse_config()
    # run sienax
    run(cmd_to_run)
 
if __name__ == "__main__":
    try:
        main()
    except Exception as exp:
        traceback_info = traceback.format_exc()
        stdout_log.error("There was an exception: %s Check stderr.log for more details. \n",exp)
        stderr_log.error("Exception: %s %s \n",exp,traceback_info)
        sys.exit(1)
