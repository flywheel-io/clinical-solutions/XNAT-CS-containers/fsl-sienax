import subprocess
import sys
import os
import csv
from bs4 import BeautifulSoup

from .xnat_logging import stdout_log

def check_options(cmd_list,cmd_options):
    """Checks if user provided options are available

    Args:
        cmd_list (list): command to run
        cmd_options (list): user provided options
    """
    # run the command, by default it will return the help menu
    process = subprocess.Popen(
                    cmd_list,
                    stdout=subprocess.PIPE,
                    stderr=subprocess.PIPE,
                    universal_newlines=True
                )
            
    stdout, stderr = process.communicate()
    process.wait()

    # bet logs help to stdout, fast logs help to stderr
    # check if user provided options exist:
    if stdout:
        for opt in cmd_options:
            if opt not in stdout:
                stdout_log.error(f"{opt} is not a possible option for {''.join(cmd_list)}")
                sys.exit(1)
        
    if stderr:
        for opt in cmd_options:
            if opt not in stderr:
                stdout_log.error(f"{opt} is not a possible option for {''.join(cmd_list)}")
                sys.exit(1)

def get_nifti_file(nifti_dir="/input/NIFTI"):
    """Get input nifti image path

    Args:
        input_dir (str, optional): path to search under. Defaults to "/input/NIFTI".
    Returns:
        str: string path to NIFTI file 
    """
    
    if not os.path.exists(nifti_dir):
        stdout_log.error("This scan does not have a NIFTI resource!")
        sys.exit(1)
    else:   
        nifti_resource_files= os.listdir(nifti_dir)
        nii_files = [file for file in nifti_resource_files if ".nii.gz" in file or ".nii" in file]

        # Assuming that there is only 1 file under NIFTI resource. 
        #  This should be the case for T1, T2 datasets
        if len(nii_files) > 0:
            nifti_file = os.path.join(nifti_dir,nii_files[0])
            return nifti_file
        elif len(nii_files) == 0:
            stdout_log.error("No files under NIFTI resource!")
            sys.exit(1)

def extract_metrics(html_report_path):
    """Extract volumetrics to CSV file

    Args:
        html_report_path (str): path to the sienax report
    """

    stdout_log.info("Parsing report.html for metrics...")
    # Read in html report
    with open(html_report_path, 'r') as html_file:
        html_content = html_file.read()

    html_soup = BeautifulSoup(html_content, 'html.parser')

    # Find the <pre> tag containing the metrics table
    table = html_soup.find('pre')
    table_contents = table.get_text()
    table_lines = table_contents.strip().split('\n')

    output_path= os.path.dirname(html_report_path)
    csv_file_path = os.path.join(output_path,'sienax_metrics.csv')

    # write metrics to a CSV file
    with open(csv_file_path, 'w', newline='') as csvfile:
        csv_writer = csv.writer(csvfile)
        csv_writer.writerow(['tissue', 'volume', 'unnormalised-volume']) # File header/column names
        for line in table_lines[1:]: # start at 1, skip header
            data = line.split()
            csv_writer.writerow(data)

    if os.path.exists(csv_file_path):
        stdout_log.info("Saved metrics to sienax_metrics.csv")
    else:
        stdout_log.error("There was an error in saving the metrics to sienax_metrics.csv!")
        sys.exit(1)
