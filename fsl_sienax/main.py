import os
import sys
import subprocess
from .xnat_logging import stdout_log,stderr_log
from .util import extract_metrics

def run(cmd_list):
   """Run sienax

   Args:
       cmd_list (list): list of options to pass to sienax
   """
   
   stdout_log.info(f"Running {' '.join(cmd_list)}")
   process = subprocess.Popen(
               cmd_list,
               stdout=subprocess.PIPE,
               stderr=subprocess.PIPE,
               universal_newlines=True
         )
      
   # Stream and log the stdout
   for line in process.stdout:
      stdout_log.info(line.strip())

   process.wait()
   return_code = process.returncode
   
   if return_code != 0:
      stdout_log.error(f"sienax returned a non-zero return code of {return_code}. Exiting! Check standard error log for more details.")
      for line in process.stderr:
         stderr_log.error(line.strip())
      sys.exit(1)

   # Get the output path right after "-o"
   index_of_o = cmd_list.index("-o")
   output_folder = cmd_list[index_of_o + 1]
   html_report_path=os.path.join(output_folder,"report.html")

   # extract volumes into a csv file
   extract_metrics(html_report_path)