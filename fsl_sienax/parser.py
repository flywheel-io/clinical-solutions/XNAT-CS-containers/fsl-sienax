"""Parser module to parse command line args from command.json"""
import os
import sys
import argparse
import datetime
from .xnat_logging import stdout_log
from .util import check_options, get_nifti_file

parser = argparse.ArgumentParser()

parser.add_argument('--input_dir', required=True, type=str,help="Input directory")
parser.add_argument('--output_dir', required=True, type=str,help="Output directory")
parser.add_argument('-lm', required=False, type=str,help="Lesion mask")
parser.add_argument('-B', required=False, type=str,help="BET options")
parser.add_argument('-S', required=False, type=str,help="FAST options")

parser.add_argument('-t', required=False, type=float,help="ignore t mm from top")
parser.add_argument('-b', required=False, type=float,help="ignore b mm from bottom")

parser.add_argument('-d', required=False, action='store_true',help="Don't delete intermediate files")
parser.add_argument('-t2', required=False, action='store_true',help="Tell FAST that input is T2w, not T1w")
parser.add_argument('-2', required=False, action='store_true',help="Do single class segmentation")
parser.add_argument('-r', required=False, action='store_true',help="Estimate regional volumes")

args=parser.parse_args() 

def parse_config():
    """Parse inputs and config
    Returns:
        list: sienax command and options as a list
    """

    # sienax command
    cmd_list=["sienax"]

    # find nifti file and full path to it
    nifti_file=get_nifti_file(f"{args.input_dir}/NIFTI")
    nifti_file_base=os.path.basename(nifti_file)
    input_file_name_prefix=nifti_file_base.split(".")[0]

    cmd_list.append(nifti_file)

    # output folder name
    output_base=f"{input_file_name_prefix}_sienax"

    timestamp = datetime.datetime.now()
    timestamp_str = timestamp.strftime("%m-%d-%Y_%H:%M:%S")

    output_folder=f"{os.path.join(args.output_dir,output_base)}_{timestamp_str}"
    cmd_list.extend(["-o",output_folder])

    # check options in BET_options. If they're invalid, then throw error.
    BET_options=args.B
    if BET_options:
        # Parse bet_opts to extract options
        options_in_bet_opts = []
        skip_next = False
        for opt in BET_options.split():
            if skip_next:
                skip_next = False
                continue
            # special case with -g. it can accept negative values, -g -0.3. -0.3 would be considered an option if we only check for "-".
            if opt.startswith("-g"):
                options_in_bet_opts.append(opt)
                skip_next = True
            elif opt.startswith("-"):
                options_in_bet_opts.append(opt.strip())

        check_options(["bet"],options_in_bet_opts)
        cmd_list.extend(["-B",BET_options])

    # check options in FAST_options. If they're invalid, then throw error.
    FAST_options=args.S
    if FAST_options:
        options_in_fast_opts = [opt.strip() for opt in FAST_options.split() if opt.startswith("-")]
        check_options(["fast"],options_in_fast_opts)
        cmd_list.extend(["-S",FAST_options])

    # check if Lesion mask file found, if not throw error
    lesion_mask=args.lm
    if lesion_mask:
        lesion_mask_path=os.path.join(f"{args.input_dir}/masks",lesion_mask.strip())
        if not os.path.exists(lesion_mask_path):
            stdout_log.error(f"Lesion mask provided: {lesion_mask_path} was not found. Exiting!")
            sys.exit(1)
        else:
            cmd_list.extend(["-lm",lesion_mask_path])
            
    # Build up sienax command with remaining options
    for arg in vars(args):
        argument_name = arg
        argument_value = getattr(args, arg)
        
        if isinstance(argument_value, bool):         
            if argument_value:
                cmd_arg=f"-{argument_name}"
                cmd_list.append(cmd_arg)
        
        if argument_name in ["t", "b"]:
            if argument_value:
                cmd_arg=f"-{argument_name}"
                cmd_list.extend([cmd_arg,str(argument_value)])

    return cmd_list