# fsl-sienax

This scan-level XNAT container wraps v6.0.7.7 FSL's [SIENAX](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/SIENA), which estimates the total brain tissue volume for a single anatomical image.


- [fsl-sienax](#fsl-sienax)
  - [Summary](#summary)
    - [Inputs](#inputs)
    - [Config](#config)
    - [Outputs](#outputs)
    - [Citation](#citation)

## Summary
- The input must be a 3D NIFTI file (.nii or .nii.gz) that is saved as a scan resource under the NIFTI folder. The container will specifically look for a folder called NIFTI under the scan, and assumes that there is only 1 NIFTI file (`fsl-sienax` will always take the first .nii or .nii.gz file found as the input file).
-  To include the optional [Lesion_mask](#inputs) input save the mask as a scan resource under **masks:**
   - <img src="./images/masks_resource.png" width=25% />
- If `fsl-sienax` is re-run on scan, pre-existing outputs are not overwritten. Instead any outputs are saved under their own [timestamped folder](#outputs).
- LICENSING NOTE: FSL software are owned by Oxford University Innovation and license is required for any commercial applications. For commercial licence please contact fsl@innovation.ox.ac.uk. For academic use, an academic license is required which is available by registering on the FSL website. Any use of the software requires that the user obtain the appropriate license. See https://fsl.fmrib.ox.ac.uk/fsldownloads_registration for more information.

- Performance:
    - `reserve-memory` in the command.json was set to 2000 (2GB)
    - `fsl-sienax`'s performance will vary on the resolution, quality, field of view etc of the image.

### Inputs
- *Anatomical image*
  - __Type__: *NIFTI*
  - __Description__: *Anatomical T1 or T2 NIFTI image saved under NIFTI scan resource*.
  - __Note__: *Enable T2 config if run on T2 image.*
- *Lesion_mask*
  - __Type__: *String*
  - __Description__: *Name of lesion (or lesion+CSF) mask file that is used to remove incorrectly labelled 'gray matter' voxels. This mask must be saved under the 'masks' scan resource. The full filename must be provided encapsulated by quotes, e.g, 'the_lesion_mask.nii.gz'. Corresponds to -lm in sienax.*
- *BET_options*
  - __Type__: *String*
  - __Description__: *Options to pass to BET, encapsulated by quotes. e.g,'-f 0.3 -g 0.6'. Corresponds to -B in sienax.*
- *FAST_options*
  - __Type__: *String*
  - __Description__: *Options to pass to FAST segmentation, encapsulated by quotes. e.g,'-I 20'. Corresponds to -S in sienax*.
- *Ignore_top*
  - __Type__: *Number*
  - __Description__: *Ignore from t (mm) upwards in MNI152/Talairach space. If you need to ignore the top part of the head (e.g. if some subjects have the top missing and you need consistency across subjects). Corresponds to -t in sienax.*
- *Ignore_bottom*
  - __Type__: *Number*
  - __Description__: *Ignore from b (mm) downwards in MNI152/Talairach space (b should probably be negative). Corresponds to -b in sienax.*

### Config
- *T2*
  - __Type__: *Boolean*
  - __Default__: *False*
  - __Description__: *Tell FAST that the input scan is T2w, not T1w. Corresponds to -t2 in sienax.*
- *Debug*
  - __Type__: *Boolean*
  - __Default__: *False*
  - __Description__: *Don't delete intermediate files. Corresponds to -d in sienax.*
- *Single_class_segmentation*
  - __Type__: *Boolean*
  - __Default__: *False*
  - __Description__: *Don’t segment gray and white matter, and segment as a single class (gray and white together). This is a good option if the gray-white matter contrast is poor. Corresponds to -2 in sienax*
- *Regional*
  - __Type__: *Boolean*
  - __Default__: *False*
  - __Description__: *Tell SIENAX to estimate 'regional' volumes as well as global; this produces peripheral cortex GM volume (3-class segmentation only) and ventricular CSF volume. Corresponds to -r in sienax.*


### Outputs
- Outputs are stored under the **SIENAX** scan resource. Everytime `fsl-sienax` is run, a new sub-folder (date & timestamped to UTC timezone) is created with the outputs pertaining to that run:
  - <img src="./images/output_fs.png" width=25% />
  - [SIENAX](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/SIENA/UserGuide#SIENAX_-_Single-Time-Point_Estimation) wiki page describes the main output files that are saved under these folders
  - Additionally, volume metrics that are displayed in the *report.html* file are extracted and saved to **sienax_metrics.csv**.
  
### Citation

- [Smith 2002] S.M. Smith, Y. Zhang, M. Jenkinson, J. Chen, P.M. Matthews, A. Federico, and N. De Stefano. Accurate, robust and automated longitudinal and cross-sectional brain change analysis. NeuroImage, 17(1):479-489, 2002.

- [Smith 2004] S.M. Smith, M. Jenkinson, M.W. Woolrich, C.F. Beckmann, T.E.J. Behrens, H. Johansen-Berg, P.R. Bannister, M. De Luca, I. Drobnjak, D.E. Flitney, R. Niazy, J. Saunders, J. Vickers, Y. Zhang, N. De Stefano, J.M. Brady, and P.M. Matthews. Advances in functional and structural MR image analysis and implementation as FSL. NeuroImage, 23(S1):208-219, 2004.

