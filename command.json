{
  "name": "fsl-sienax",
  "label": "fsl-sienax",
  "description": "Runs fsl-sienax v1.0. LICENSING NOTE: FSL software are owned by Oxford University Innovation and license is required for any commercial applications. For commercial licence please contact fsl@innovation.ox.ac.uk. For academic use, an academic license is required which is available by registering on the FSL website. Any use of the software requires that the user obtain the appropriate license. See https://fsl.fmrib.ox.ac.uk/fsldownloads_registration for more information.",
  "version": "1.0",
  "schema-version": "1.0",
  "image": "registry.gitlab.com/flywheel-io/clinical-solutions/xnat-cs-containers/fsl-sienax:1.0",
  "type": "docker",
  "command-line": "python run.py --input_dir /input --output_dir /output #T2# #LESION_MASK# #DEBUG# #BET_OPTS# #CLASS_SEG# #IGNORE_TOP# #IGNORE_BOTTOM# #REGIONAL# #FAST_OPTS#",
  "override-entrypoint": true,
  "mounts": [
    {
      "name": "in",
      "writable": false,
      "path": "/input"
    },
    {
      "name": "out",
      "writable": true,
      "path": "/output"
    }
  ],
  "environment-variables": {},
  "ports": {},
  "inputs": [
    {
      "name": "T2",
      "description": "Tell FAST that the input scan is T2w, not T1w. Corresponds to -t2 in sienax.",
      "type": "boolean",
      "default-value": "false",
      "required": false,
      "replacement-key": "#T2#",
      "true-value": "-t2",
      "false-value": "",
      "select-values": []
    },
    {
      "name": "Debug",
      "description": "Don't delete intermediate files. Corresponds to -d in sienax.",
      "type": "boolean",
      "default-value": "false",
      "required": false,
      "replacement-key": "#DEBUG#",
      "true-value": "-d",
      "false-value": "",
      "select-values": []
    },
    {
      "name": "Single_class_segmentation",
      "description": "Don’t segment gray and white matter, and segment as a single class (gray and white together). This is a good option if the gray-white matter contrast is poor. Corresponds to -2 in sienax.",
      "type": "boolean",
      "default-value": "false",
      "required": false,
      "replacement-key": "#CLASS_SEG#",
      "true-value": "-2",
      "false-value": "",
      "select-values": []
    },
    {
      "name": "Regional",
      "description": "Tell SIENAX to estimate 'regional' volumes as well as global; this produces peripheral cortex GM volume (3-class segmentation only) and ventricular CSF volume. Corresponds to -r in sienax.",
      "type": "boolean",
      "default-value": "false",
      "required": false,
      "replacement-key": "#REGIONAL#",
      "true-value": "-r",
      "false-value": "",
      "select-values": []
    },
    {
      "name": "Lesion_mask",
      "description": "Name of lesion (or lesion+CSF) mask file that is used to remove incorrectly labelled 'gray matter' voxels. This mask must be saved under the 'masks' scan resource. The full filename must be provided encapsulated by quotes, e.g, 'the_lesion_mask.nii.gz'. Corresponds to -lm in sienax.",
      "type": "string",
      "default-value": "",
      "required": false,
      "replacement-key": "#LESION_MASK#",
      "command-line-flag": "-lm",
      "command-line-separator": " ",
      "select-values": []
    },
    {
      "name": "BET_options",
      "description": "Options to pass to BET, encapsulated by quotes. e.g,'-f 0.3 -g 0.6'. Corresponds to -B in sienax.",
      "type": "string",
      "default-value": "",
      "required": false,
      "replacement-key": "#BET_OPTS#",
      "command-line-flag": "-B",
      "command-line-separator": "=",
      "select-values": []
    },
        
    {
      "name": "FAST_options",
      "description": "Options to pass to FAST segmentation, encapsulated by quotes. e.g,'-I 20'.  Corresponds to -S in sienax.",
      "type": "string",
      "default-value": "",
      "required": false,
      "replacement-key": "#FAST_OPTS#",
      "command-line-flag": "-S",
      "command-line-separator": "=",
      "select-values": []
    },
    {
      "name": "Ignore_top",
      "description": "Ignore from t (mm) upwards in MNI152/Talairach space. If you need to ignore the top part of the head (e.g. if some subjects have the top missing and you need consistency across subjects). Corresponds to -t in sienax.",
      "type": "number",
      "default-value": "",
      "required": false,
      "replacement-key": "#IGNORE_TOP#",
      "command-line-flag": "-t",
      "command-line-separator": " ",
      "select-values": []
    },
    {
      "name": "Ignore_bottom",
      "description": "Ignore from b (mm) downwards in MNI152/Talairach space (b should probably be negative). Corresponds to -b in sienax.",
      "type": "number",
      "default-value": "",
      "required": false,
      "replacement-key": "#IGNORE_BOTTOM#",
      "command-line-flag": "-b",
      "command-line-separator": "=",
      "select-values": []
    }
  ],
  "outputs": [
    {
      "name": "fsl-sienax",
      "description": "fsl-sienax output",
      "required": true,
      "mount": "out",
      "glob": ""
    }
  ],
  "xnat": [
    {
      "name": "fsl-sienax",
      "label": "Runs fsl-sienax-v1.0",
      "description": "Run fsl-sienax v1.0",
      "contexts": [
        "xnat:imageScanData"
      ],
      "external-inputs": [
        {
          "name": "scan",
          "description": "Input scan",
          "type": "Scan",
          "required": true,
          "provides-files-for-command-mount": "in",
          "load-children": false
        }
      ],
      "derived-inputs": [],
      "output-handlers": [
        {
          "name": "fsl-sienax",
          "accepts-command-output": "fsl-sienax",
          "as-a-child-of": "scan",
          "type": "Resource",
          "label": "SIENAX",
          "tags": []
        }
      ]
    }
  ],
  "reserve-memory": 2000,
  "container-labels": {},
  "generic-resources": {},
  "ulimits": {},
  "secrets": []
}